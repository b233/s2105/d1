let http = require ("http");

const PORT = 3000;

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Robert",
		"email": "robert@mail.com"
	}
	];

http.createServer ((req, res)=>{
 if( req.url === "/users" && req.method === "GET"){
 	res.writeHead(200, {"Content-Type": "application/json"});
 	// console.log(directory)
 	res.write(JSON.stringify(directory));
 	res.end();
	}	
		// console.log(req);
	if(req.url === "/users" && req.method === "POST"){

		let reqBody = "";

		req.on("data", (data)=>{
			// console.log(data)
			reqBody += data;
		});
		req.on("end",()=>{
			console.log(typeof reqBody);
			reqBody = JSON.parse(reqBody);
			console.log(reqBody);

			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}
			directory.push(newUser);
			console.log(directory)

			res.writeHead(200, 
				{"Content-Type": "application/jason"});
			res.write(JSON.stringify(directory));
			res.end();
		});
	}
}).listen(PORT);

console.log(`Server is now connected to ${PORT}`) 